const mysqlConnection = require('../database');

const update_product = (req, res) => {
    console.log(req.body)
    console.log(req.params)
    mysqlConnection.query('UPDATE TT_PRODUCTO SET ? WHERE id_producto = ?', [req.body, req.params.id], (err, results, fields) => {
        if (!err) {
            console.log(results)
            res.status(200).json(results);
        } else {
            console.log(err);
            res.status(500).json(err);
        }
    });
}

module.exports = {
    update_product: update_product
}