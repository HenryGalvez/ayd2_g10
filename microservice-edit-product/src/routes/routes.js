var express = require('express');
var router = express.Router();

const product_controller = require('../controllers/product_controllers');

router.put('/products/update-producto/:id',product_controller.update_product)

module.exports = router;