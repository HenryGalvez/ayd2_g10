'use stricts'

let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3000';


//Prueba correcta
describe('update the description of product with id 1: ',()=>{
    it('should update the description of products', (done) => {
    chai.request(url)
    .put('/products/update-producto/1')
    .send({ descripcion: "Descripcion producto de prueba"})
    .end( function(err,res){
    console.log(res.body)
    expect(res.body).to.have.property('affectedRows').to.be.equal(1);
    expect(res).to.have.status(200);
    done();
    });
    });
});


//Prueba incorrecta







