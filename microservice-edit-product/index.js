'use strict';

require('dotenv').config();

var logger = require('morgan');
const express = require('express');
const port = process.env.PORT || 3000;
const path = require('path');
var cors = require('cors')

var principal = require('./src/routes/routes')


const app = express();

app.use(cors())
app.use(logger('dev'))


//AGREGAMOS LAS DOS LINEAS SIGUIENTES

app.use(express.urlencoded({ extended: false }));
app.use(express.json());


app.use('/', principal);

app.get('/principal', function(req, res) {
    res.send('FUNCIONANDO CORRECTAMENTE');
  });


app.listen(port, function() {
    console.log('Servidor edit corriendo en el puerto ' + port);
});