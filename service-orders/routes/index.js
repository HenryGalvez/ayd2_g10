const conn = require("../private/database");
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/get-logs', function (req, res, next) {
  const query = 'select * from tt_log'
    conn.query(query, (err, results, fields) => {
      if(err){
        res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
      }
      res.status(200).json({data: results})
    })
});

module.exports = router;
