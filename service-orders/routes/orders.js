const conn = require("../private/database");
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('Services Orders');
});

router.post('/create-order', function (req, res, next) {
  var {
    direccion,
    id_tipo_entrega,
    id_usuario,
    id_estado_orden
  } = req.body;
  const query = `insert into TT_ORDEN(fecha_hora, direccion, id_tipo_entrega, id_usuario, id_estado_orden,estado_registro) values(now(), '${direccion}', ${id_tipo_entrega}, ${id_usuario}, ${id_estado_orden},1)`
    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: err.message});
      }
      conn.query("select last_insert_id()", (err, rows, fields) => {
        if(err){
          res.status(500).json({ message: err.message});
        }
        res.status(200).json({message: "Se registró correctamente la orden", id: rows[0]['last_insert_id()']})
        conn.query(`insert into tt_log(id_tipo_log,descripcion,id_usuario_realizo,fecha_hora) values(1,"Se creo una nueva orden",${id_usuario}, now())`, (err, rows, fields) => { })
      })
    })
});

router.post('/insert-item-order', function (req, res, next) {
  var {
    id_orden,
    id_producto
  } = req.body;
  const query = `insert into TT_DETALLE_ORDEN(id_orden, id_producto) values(${id_orden}, ${id_producto})`
    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: err.message});
      }
      conn.query("select last_insert_id()", (err, rows, fields) => {
        if(err){
          res.status(500).json({ message: err.message});
        }
        res.status(200).json({message: "Se registró correctamente el item dentro de la orden", id: rows[0]['last_insert_id()']})
      })
    })
});

router.get('/get-orders/:id_user', function (req, res, next) {
  var {
    id_user,
  } = req.params;
  const query = `select * from TT_ORDEN to2,TC_ESTADO_ORDEN teo where 
                 to2.id_estado_orden = teo.id_estado_orden
                 and to2.id_usuario =  ${id_user}`

    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
      }
      res.status(200).json({data: rows})
    })
});


router.get('/get-orders-restaurant/:id_restaurant', function (req, res, next) {
  var {
    id_restaurant,
  } = req.params;
  const query = `SELECT to2.id_orden, to2.direccion,to2.fecha_hora, teo.descripcion,
	               tu.nombre, tu.id_usuario, tu.telefono, to2.id_tipo_entrega,to2.estado_registro 
                 from TT_DETALLE_ORDEN tdo, TT_ORDEN to2,
	               TT_PRODUCTO tp, TC_ESTADO_ORDEN teo,
	               TT_USUARIO tu 
                 where tdo.id_orden=to2.id_orden 
                 and tp.id_producto =tdo.id_producto
                 and teo.id_estado_orden = to2.id_estado_orden 
                 and tp.id_restaurante =${id_restaurant}
                 and tu.id_usuario = to2.id_usuario 
                 GROUP by to2.id_orden`

    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
      }
      res.status(200).json({data: rows})
    })
});


router.get('/get-detail-order/:id_order', function (req, res, next) {
  var {
    id_order,
  } = req.params;
  const query = `SELECT * from TT_DETALLE_ORDEN tdo, TT_ORDEN to2,TT_PRODUCTO tp 
                 where tdo.id_orden=to2.id_orden 
                 and tp.id_producto =tdo.id_producto  
                 and to2.id_orden = ${id_order}`

    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
      }
      res.status(200).json({data: rows})
    })
});


router.get('/get-all-orders', function (req, res, next) {

  const query = `select * from TT_ORDEN to2,TC_ESTADO_ORDEN teo where 
                 to2.id_estado_orden = teo.id_estado_orden`

    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
      }
      res.status(200).json({data: rows})
    })
});





router.put('/update-order/:id', function (req, res, next) {

  conn.query('UPDATE TT_ORDEN SET ? WHERE id_orden = ?', [req.body, req.params.id], (err, results, fields) => {
    if (!err) {
        console.log(results)
        res.status(200).json(results);
        conn.query(`insert into tt_log(id_tipo_log,descripcion,id_usuario_realizo,fecha_hora) values(1,"Se actualizo la orden ${req.params.id}",${req.body.id_usuario}, now())`, (err, rows, fields) => { })


    } else {
        console.log(err);
        res.status(500).json(err);
    }
  });

});


router.delete('/delete-order/:id', function (req, res, next) {
  var id_user;
  conn.query(`SELECT *FROM tt_orden WHERE id_orden  = ?`,[req.params.id],
          (err,results,fields)=>{
            id_user = results[0].id_usuario;
          })

  conn.query('DELETE FROM TT_ORDEN  WHERE id_orden = ?', [req.params.id], (err, results, fields) => {
    if (!err) {
        console.log(results)
        res.status(200).json(results);
        conn.query(`insert into tt_log(id_tipo_log,descripcion,id_usuario_realizo,fecha_hora) values(1,"Se elimino la orden ${req.params.id} del usuario ${id_user}",${id_user}, now())`, (err, rows, fields) => { })
    
    } else {
        console.log(err);
        res.status(500).json(err);
    }
  });
});






module.exports = router;
