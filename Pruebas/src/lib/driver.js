// Pruebas locales

const { Builder, By, until, Key } = require('selenium-webdriver');
//const { FileDetector } = require('selenium-webdriver');

SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;

var cbtHub = `http://3.141.21.96:4444/wd/hub`

const chrome = require('selenium-webdriver/chrome');
const firefox = require('selenium-webdriver/firefox')
const chromedriver = require('chromedriver');



/* Pruebas remotas*/
//let options = new chrome.Options();
let options = new firefox.Options();

options.addArguments('disable-infobars');
//options.addArguments("start-maximized");
//options.addArguments('--headless'); // running test on visual chrome browser
//options.setUserPreferences({ credential_enable_service: false });

//options.addExtensions('/path/to/firebug.xpi')
//options.setPreference('extensions.firebug.showChromeErrors', true);




var caps = {
    name: 'Chrome Test',
    setPageLoadStrategy: 'eager',
    browserName: 'chrome',
    browserVersion: '89.0.4389.82'
};
/*
var caps = {
    name: 'Firefox Test',
    setPageLoadStrategy: 'eager',
    browserName: 'firefox',
    browserVersion: '71.0'
};*/


var Page = function() {

    this.driver = new Builder()
    //this.driver.setFileDetector(new FileDetector())
    //.setChromeOptions(options)
    .withCapabilities(caps)
    .usingServer(cbtHub)
    //.forBrowser('chrome')
    //Para firefox
    //.forBrowser('firefox')
     //.setFirefoxOptions(options)
    .build();


    // visit a webpage
    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    };


    // quit current session
    this.quit = async function() {
        return await this.driver.quit();
    };


 // wait and find a specific element with it's id
 this.findById = async function(id) {
    await this.driver.wait(until.elementLocated(By.id(id)), 15000, 'Looking for element');
    return await this.driver.findElement(By.id(id));
};

 // wait and find a specific element with it's name
 this.findByName = async function(name) {
    await this.driver.wait(until.elementLocated(By.name(name)), 15000, 'Looking for element');
    return await this.driver.findElement(By.name(name));
};

this.findByCss = async function(css){
    await this.driver.wait(until.elementLocated(By.css(css)), 15000, 'Looking for element');
    return await this.driver.findElement(By.css(css));
}

this.findByXPath = async function(path){
    await this.driver.wait(until.elementLocated(By.xpath(path)), 15000, 'Looking for element');
    return await this.driver.findElement(By.xpath(path));
}

 // fill input web elements
this.write = async function(el, txt) {
    return await el.sendKeys(txt);
};

};

module.exports = Page;


/*
// Pruebas automatizadas con Selenium
const { Builder, By, until, Key } = require('selenium-webdriver');

SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
var cbtHub = `http://${process.env.HUB_HOST}:4444/wd/hub`

//var cbtHub = `http://18.218.150.45:4444/wd/hub`

var caps = {
    name: 'Chrome Test',
    setPageLoadStrategy: 'eager',
    browserName: 'chrome',
    browserVersion: '89.0.4389.82'
};



var Page = function() {

    this.driver = new Builder()
    //.setChromeOptions(options)
    .withCapabilities(caps)
    .usingServer(cbtHub)
    //.forBrowser('chrome')
    .build();


    // visit a webpage
    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    };


    // quit current session
    this.quit = async function() {
        return await this.driver.quit();
    };


 // wait and find a specific element with it's id
 this.findById = async function(id) {
    await this.driver.wait(until.elementLocated(By.id(id)), 15000, 'Looking for element');
    return await this.driver.findElement(By.id(id));
};

 // wait and find a specific element with it's name
 this.findByName = async function(name) {
    await this.driver.wait(until.elementLocated(By.name(name)), 15000, 'Looking for element');
    return await this.driver.findElement(By.name(name));
};


 // fill input web elements
this.write = async function(el, txt) {
    return await el.sendKeys(txt);
};




};

module.exports = Page;*/
