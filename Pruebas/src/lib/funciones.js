
let Page = require('./driver');

const fake = require('../utils/fakeData');
const fakeNameKeyword = fake.nameKeyword;
const fakePassKeyword = fake.passFake;

let searchInput, searchButton, resultStat;

let botonlogin, searchInputName, searchInputPassword, resultadouser;

//Variables para prueba a productos
let searchBotonNuevo, searchInputProductName, searchInputDescription, searchInputPrecio, searchSelectRestaurant;

Page.prototype.findInputAndButton = async function() {
    searchInput = await this.findByName("q");
    searchButton = await this.findByName("btnK");

    const result = await this.driver.wait(async function() {
        const searchInputEnableFlag = await searchInput.isEnabled();
        const searchButtonText = await searchButton.getAttribute('value');
        return {
            inputEnabled: searchInputEnableFlag,
            buttonText: searchButtonText
        }
    }, 5000);
    return result;
};


Page.prototype.submitKeywordAndGetResult = async function() {
    await this.findInputAndButton();
    await this.write(searchInput, fakeNameKeyword);
    await searchInput.sendKeys("\n");

    resultStat = await this.findById("result-stats");
    return await this.driver.wait(async function() {
        return await resultStat.getText();
    },5000);
};

// Login correcto
Page.prototype.findElementsLogin = async function(){
    //botonlogin = await (await this.findByXPath("/html/body/app-root/div/app-header/header/nav/ul/li[2]/a")).click();
    //searchInputName = await this.findByName("userName");
    searchInputName = await this.findByXPath('//*[@id="userName"]')
    //searchInputPassword = await this.findByName("password");
    searchInputPassword = await this.findByXPath('//*[@id="password"]')
    
    

    const result = await this.driver.wait(async function() {
    const searchInputNameEnableFlag = await searchInputName.isEnabled();
    const searchInputPassEnableFlag = await searchInputPassword.isEnabled();
        
    return {
        inputEnabled: searchInputNameEnableFlag,
        inputEnabledPass: searchInputPassEnableFlag
    }
    }, 20000);
    return result;
};

Page.prototype.submitAuth = async function() {
    await this.findElementsLogin();
    botonlogin = await (await this.findByXPath("/html/body/app-root/div/app-header/header/nav/ul/li[2]/a")).click();
    await this.write(searchInputName,"admin");
    await this.write(searchInputPassword,"123");
    searchButton = await (await this.findById("btningresar")).click();
    

    resultadouser = await (await this.findByXPath('//*[@id="username"]')).getText();
    return await this.driver.wait(async function() {
        return await resultadouser;
    },30000);
};



// Login incorrecto
Page.prototype.submitFailAuth = async function() {
    await this.findElementsLogin();
    botonlogin = await (await this.findByXPath("/html/body/app-root/div/app-header/header/nav/ul/li[2]/a")).click();
    await this.write(searchInputName, fakeNameKeyword);
    await this.write(searchInputPassword, "fail")
    searchButton = await (await this.findById("btningresar")).click();
    
    resultadouser = await this.findById("msg")
    //resultadouser = await (await this.findByXPath('//*[@id="modal"]/div/div/form/div[1]/div/h4[3]')).getText();
    return await this.driver.wait(async function() {
       return await resultadouser.getText();
    },20000);
};




// Busqueda componentes crear producto


Page.prototype.findElementsProduct = async function(){
    searchBotonNuevo = await (await this.findByXPath("/html/body/app-root/app-product/div/div/div/div/button")).click();
    searchInputProductName = await this.findByName("name");
    searchInputPrecio = await this.findByName("price");
    searchInputDescription = await this.findByName("description")
   
    

    const result = await this.driver.wait(async function() {
        const searchInputNameEnableFlag = await searchInputProductName.isEnabled();
        const searchInputPriceEnableFlag = await searchInputPrecio.isEnabled();
        const searchInputDescriptionFlag = await searchInputDescription.isEnabled();
        
        
        return {
            inputNameEnabled: searchInputNameEnableFlag,
            inputPriceEnabled: searchInputPriceEnableFlag,
            inputDescriptionEnabled : searchInputDescriptionFlag,
           
        }

    }, 5000);
    return result;
};


Page.prototype.submitCorrectProduct = async function() {
    await this.findElementsProduct();
    await this.write(searchInputProductName, "Hot And Ready");
    await this.write(searchInputPrecio, "40")
    await this.write(searchInputDescription, "Pizza de jamon o peperoni")

    //searchButton = await (await this.findByXPath("/html/body/app-root/app-product/div/div/form/div/div/div/div/div[1]/button[1]")).click();
    
    //resultadoproduct = await (await this.findByXPath('//*[@id="swal2-content"]')).getText();
    return await this.driver.wait(async function() {
       return  "correcto"
    },50000);
};




// producto incorrecto
Page.prototype.submitFailProduct = async function() {
    await this.findElementsProduct();
    /*await this.write(searchInputProductName, "");
    await this.write(searchInputPrecio, "")
    await this.write(searchInputDescription, "")*/

    searchButton = await (await this.findByXPath("/html/body/app-root/app-product/div/div/form/div/div/div/div/div[1]/button[1]")).click();
    
    resultadoproduct = await (await this.findByXPath('//*[@id="swal2-content"]')).getText();
    return await this.driver.wait(async function() {
       return await resultadoproduct;
    },50000);
};




module.exports = Page;
