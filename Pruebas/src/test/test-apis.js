
'use stricts'

let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const endpoint1= 'http://18.224.215.118:3014';
const endpoint2= 'http://18.224.215.118:3015';
const endpoint3= 'http://18.224.215.118:3012';
const endpoint4= 'http://18.224.215.118:3008';




describe('Unit test - correct login: ',()=>{
    it('should success the login', (done) => {
    chai.request(endpoint4)
    .post('/users/login-user')
    .send({ usuario: "admin",password: "123"})
    .end( function(err,res){
    //console.log(res)
    expect(res).to.have.status(200);
    done();
    });
    });
});




describe('Unit test - get the product with id 1: ',()=>{
    it('should get the product with id 1', (done) => {
    chai.request(endpoint1)
    .get('/products/get-product/1')
    .end( function(err,res){
    let data = res.body;
    //console.log(data.data)
    expect(data.data).to.have.property('id_producto').to.be.equal(1);
    expect(res).to.have.status(200);
    done();
    });
    });
});


describe('Unit test - get all products: ',()=>{
    it('should get all products', (done) => {
    chai.request(endpoint2)
    .get('/list_products/list/products')
    .end( function(err,res){
    //console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
});

describe('Unit test - get all restaurants: ',()=>{
    it('should get all restaurants', (done) => {
    chai.request(endpoint2)
    .get('/list_products/list/restaurants')
    .end( function(err,res){
    // console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
});
/*
describe('Unit test - update the description of product with id 1: ',()=>{
    it('should update the description of products', (done) => {
    chai.request(endpoint3)
    .put('/products/update-producto/1')
    .send({ 
      nombre:"Pizza de Peperonni",
      precio:70,
      descripcion:"Pizza con peperonnis prueba 1 ",
      fotografia:"Fotos_Productos/Pizza de Peperonni-7181ac17-e574-44d9-9ff7-d1c3df04fbe0.png",
      id_restaurante: 3,
      oferta:60
    })
    .end( function(err,res){
    //console.log(err);
    console.log(res.body);
    expect(res.body).to.have.property('affectedRows').to.be.equal(1);
    expect(res).to.have.status(200);
    done();
    });
    });
});
*/

describe('Unit test - Registro de un nuevo usuario: ',()=>{
    it('should insert a user',  (done) => {
    chai.request(endpoint4)
    .post('/users/signup-user')
    .send({nombre:"User Test", telefono: "12345678", direccion: "Testing city", usuario: 'test', password: 'test', id_rol:1})
    .end( function(err,res){
    //console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
});



