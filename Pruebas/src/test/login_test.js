/*
const { describe, it, after, before } = require('mocha');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);


const Page = require('../lib/funciones');
process.on('unhandledRejection', () => {});



(async function correct_auth() {
    try{

        describe('Testeo automatizado para el autenticacion correcta de un usuario', async function() {
            this.timeout(50000);
            var driver, page;

            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                await page.visit('http://18.224.215.118:8080/dashboard');
                //await page.visit('http://localhost:4200/dashboard');
                

            });

            afterEach(async() => {
                await page.quit();
            });

            it('Buscar la caja texto del usuario , password y el boton de ingresar', async() => {                
                const result = await page.findElementsLogin();
                expect(result.inputEnabled).to.equal(true);
                expect(result.inputEnabledPass).to.equal(true);
                //expect(result.buttonTextI).to.equal('Ingresar');
            });

            it('Mostrar el nombre del usuario que ingreso si su inicio de sesion fue correcto', async() => {
                const result = await page.submitAuth();
                console.log(result)
                expect(result).to.be.equal('admin');
                
            });


        });

    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();

(async function incorrect_auth() {
    try{

        describe('Testeo automatizado para el autenticacion incorrecta de un usuario', async function() {
            this.timeout(50000);
            var driver, page;

            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                await page.visit('http://18.224.215.118:8080/dashboard');
                //await page.visit('http://localhost:4200/dashboard');

            });

            afterEach(async() => {
                await page.quit();
            });

            it('Buscar la caja texto del usuario , password y el boton de ingresar', async() => {                
                const result = await page.findElementsLogin();
                expect(result.inputEnabled).to.equal(true);
                expect(result.inputEnabledPass).to.equal(true);
                //expect(result.buttonTextI).to.equal('Ingresar');
            });

           it('Mostrar error por datos erroneos ', async() => {
                const result = await page.submitFailAuth();
                console.log(result)
                expect(result.length).to.be.above(5);

            });

        });

    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();*/