
const { describe, it, after, before } = require('mocha');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);


const Page = require('../lib/funciones');
process.on('unhandledRejection', () => {});


(async function correct_addProduct() {
    try{
        describe('Testeo automatizado para agregar un producto nuevo', async function() {
            this.timeout(50000);
            var driver, page;

            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                await page.visit('http://18.224.215.118:8080/product');

            });

            afterEach(async() => {
                await page.quit();
            });



            it('Solicitar agregar nuevo producto y Buscar la cajas de texto a llenar', async() => {                
                const result = await page.findElementsProduct();
                expect(result.inputNameEnabled).to.equal(true);
                expect(result.inputPriceEnabled).to.equal(true);
                expect(result.inputDescriptionEnabled).to.equal(true);
                
                //expect(result.buttonTextI).to.equal('Ingresar');
            });

            it('Mostrar el producto agregado', async() => {
                //const result = await page.submitCorrectProduct();
                //console.log(result);
                

            });


        });

    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



(async function incorrect_addProduct() {
    try{
        describe('Testeo automatizado para validar error al agregar un producto nuevo', async function() {
            this.timeout(70000);
            var driver, page;

            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                await page.visit('http://18.224.215.118:8080/product');
                //await page.visit('http://localhost:4200/product');

            });

            afterEach(async() => {
                await page.quit();
            });

            it('Solicitar agregar nuevo producto y Buscar la cajas de texto a llenar', async() => {                
                const result = await page.findElementsProduct();
                expect(result.inputNameEnabled).to.equal(true);
                expect(result.inputPriceEnabled).to.equal(true);
                expect(result.inputDescriptionEnabled).to.equal(true);
                
                
            });

            it('Intentar crear un producto y mostrar error al no haber llenado los campos requeridos', async() => {
                const result = await page.submitFailProduct();
                console.log(result);
                expect(result).to.include('error');

            });




        });

    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();

