/*

'use stricts'

let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const endpoint1= 'http://3.137.208.43:3014';
const endpoint2= 'http://3.137.208.43:3010';
const endpoint3= 'http://3.137.208.43:3012';
const endpoint4= 'http://3.137.208.43:3008';
const endpoint5= 'http://3.137.208.43:3011';



describe('Unit test - login-password incorrecto: ',()=>{
    it('should fail the login', (done) => {
    chai.request(endpoint4)
    .post('/users/login-user')
    .send({ usuario: "admin",password: "1234"})
    .end( function(err,res){
    //console.log(res)
    expect(res).to.have.status(500);
    expect(res.text).to.include('{"message":"Password Incorrecta"}');
    done();
    });
    });
});


describe('Unit test - login-usuario no registrado: ',()=>{
    it('should fail the login', (done) => {
    chai.request(endpoint4)
    .post('/users/login-user')
    .send({ usuario: "admis",password: "123"})
    .end( function(err,res){
    //console.log(res)
    expect(res).to.have.status(500);
    done();
    });
    });
});


describe('Unit test - get the product with id 20: ',()=>{
    it('should get status 404 because not found the product', (done) => {
        chai.request(endpoint1)
        .get('/products/get-product/2000')
        .end( function(err,res){
            let data = res.body;
            console.log(data)
            expect(res).to.have.status(404);
            done();
        });
    });
});







describe('Unit test - Registro de un nuevo usuario: ',()=>{
    it('should insert a new user ',  (done) => {
        chai.request(endpoint4)
        .post('/users/signup-user')
        .send({ nombre:"User Test", telefono: "12345678", direccion: "Testing city", usuario: 'test', password: 'test', id_rol:"cliente"})
        .end( function(err,res){
            expect(res).to.have.status(500);
            done();
        });
    });
});



describe('Unit test - get all logs: ',()=>{
    it('should get 0 logs', (done) => {
    chai.request(endpoint2)
    .get('/users/logs')
    .end( function(err,res){
     console.log(res.body)
    expect(res).to.have.status(404);
    done();
    });
    });
});






/*
describe('Unit test - get the product with id 2: ',()=>{
    it('should get the product with id 1', (done) => {
        chai.request(endpoint1)
        .get('/products/get-product/20')
        .end( function(err,res){
            let data = res.body;
            console.log(data)
            expect(res).to.have.status(200);
            done();
        });
    });
});







/*
describe('Unit test - get the product with id 1: ',()=>{
    it('should get the product with id 1', (done) => {
        chai.request(endpoint1)
        .get('/products/get-product/1')
        .end( function(err,res){
            let data = res.body;
            console.log(data.data)
            expect(data.data).to.have.property('id').to.be.equal(1);
            expect(res).to.have.status(400);
            done();
        });
    });
});

*/

/*



*/

/*
describe('Unit test - Delete producto: ',()=>{
    it('Eliminacion de un producto no existente ', (done) => {
        chai.request(endpoint5)
        .delete('/products/delete-producto/5000')
        .end( function(err,res){
            expect(res.body).to.have.property('affectedRows').to.be.equal(1);
            expect(res).to.have.status(200);
            done()
        });   
    }); 
});

*/

/*
describe('Unit test - update the category of product with id 1: ',()=>{
    it('should update the category of products', (done) => {
        chai.request(endpoint3)
        .put('/products/update-producto/1')
        .send({ categoria: "Nueva categoria"})
        .end( function(err,res){
            console.log(res.body)
            expect(res.body).to.have.property('affectedRows').to.be.equal(1);
            expect(res).to.have.status(200);
            done();
        });
    });
});


describe('Unit test - create new order: ',()=>{
    it('should create new order', (done) => {
        chai.request(endpoint2)
        .post('/orders/create-order')
        .send({direccion:"Mixco Guatemala",id_tipo_entrega:1,id_usuario:1,id_estado_orden:1})
        .end( function(err,res){
            expect(res.body).to.have.property('affectedRows').to.be.equal(1);
            expect(res).to.have.status(400);
            done();

        });

    });

});
*/

