const faker = require('faker');

module.exports = {
    nameKeyword: faker.name.findName(),
    passFake: faker.internet.password()
};
