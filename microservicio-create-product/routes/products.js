const conn = require("../private/database");
var express = require('express');
var router = express.Router();
let uuid = require('uuid');
const aws_keys = require('../private/creds');
var AWS = require('aws-sdk');

const s3 = new AWS.S3(aws_keys.s3);

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

router.post('/create-product', function (req, res, next) {
  var {
    nombre,
    precio,
    descripcion,
    fotografia,
    extension,
    id_restaurante
  } = req.body;


  file_name = nombre +"-"+ uuid.v4() +"."+ extension;
  file_path = "Fotos_Productos/"+ file_name;
  b64_parts = fotografia.split(',')
  image_64_encode_str = b64_parts.length == 2 && b64_parts[1] || b64_parts[0];
  let buff = new Buffer.from(image_64_encode_str, 'base64');

  const params = {
    Bucket: "ayd2-img",
    Key: file_path,
    Body: buff,
    ContentType: "image",
    ACL: 'public-read'
  };

  s3.putObject(params).promise();




  console.log(file_path);
  const query = `insert into TT_PRODUCTO(nombre, precio, descripcion, fotografia, id_restaurante,oferta) values('${nombre}', ${precio}, '${descripcion}', '${file_path}', ${id_restaurante},0.0)`
  console.log(query);  
  conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: err.message});
      }
      conn.query("select last_insert_id()", (err, rows, fields) => {
        if(err){
          res.status(500).json({ message: err.message});
        }
        res.status(200).json({message: "Se registró correctamente el producto", id: rows[0]['last_insert_id()']})
      })
    })
});


module.exports = router;
