const mysqlConnection = require('../database');

const delete_product = (req, res) => {
 
    mysqlConnection.query('DELETE FROM TT_PRODUCTO  WHERE id_producto = ?', [req.params.id], (err, results, fields) => {
        if (!err) {
            console.log(results)
            res.status(200).json(results);
        } else {
            console.log(err);
            res.status(500).json(err);
        }
    });
}


module.exports = {
    delete_product: delete_product
}