var express = require('express');
var router = express.Router();

const product_controller = require('../controllers/product_controllers');

router.delete('/products/delete-producto/:id',product_controller.delete_product)

module.exports = router;