import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CRUDProductService } from 'src/app/services/crud-product.service';
import { Router } from '@angular/router'
import Swal from 'sweetalert2';
import { URL_AWS } from 'src/app/services/shared/shared_url_template';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  
  url=URL_AWS;
  ofertas:any=[]
  productos:any=[]
  MenuName:string='';
  ProductId:string='';
  MenuValue:number = 0;
  cards:any=[];
  values:any=[];
  value:any=[];
  carrito:any=[];
  detalle:any=[];
  RestauranteId=0;
  prueba:any=[];
  menu:any=[]; 
  userAnonomous:Boolean = localStorage.getItem('username') === null ? true : false;

  Restaurantes:any=[];

  public formaCantidad = this.fb.group({
    Cantidad:[1,[Validators.required,Validators.pattern("^[0-9]*$")]]
  })

  formaValida:boolean = true;
  isAdmin:Boolean = localStorage.getItem('username') === 'admin' ? true : false;

  constructor(private service:CRUDProductService,
              private service2:CRUDProductService,
              private fb:FormBuilder,
              private route:Router) { }
  
  ngOnInit(): void {

    this.service.getOferts().subscribe((data:any)=>{
      console.log(data);
      this.ofertas=data.data;
    })
    
    this.service.getProducts().subscribe((data:any)=>{
      this.productos=data.data;
    });

    if(localStorage.getItem('detalle')){
      //console.log('no existe');
      this.detalle = JSON.parse(localStorage.getItem('detalle')|| '{}');
    }
    this.pruebas();
    console.log(this.isAdmin);
    console.log(this.userAnonomous);

  }


  pruebas(){
    let restaurante:any={
      nombre:'',
      id_usuario:0,
      direccion:'',
      menu:[]
    }


    this.service2.getRestaurants().subscribe((data:any)=>{
   
      this.Restaurantes=data.data;
      let productos:any=[];



      for (let index = 0; index < this.Restaurantes.length; index++) {
       
        
        
        
        this.menu[index]=this.Restaurantes[index];
        for (let j = 0; j < this.productos.length; j++) {
          let a:string =this.productos[j].id_restaurante;
          let b:string = this.Restaurantes[index].id_usuario;

          if(a===b){
            productos[j]=this.productos[j]; 
          }
          
        }   

        this.menu[index]={ restaurante:this.menu[index],productos}
        productos=[];

  
        
      }   



      

      for (let index = 0; index < this.menu.length; index++) {
       
        for (let j = 0; j < this.menu[index].productos.length; j++) {
          let aux2=this.menu[index].productos;
          let filtered = aux2.filter(function (el:any) {
            return el != null;
          });
          this.menu[index].productos=filtered;
            
        }   

        
      }   


      

    });


  }


  defaultCantidad(producto:any){


    console.log(producto);
    this.ProductId = producto.id_producto;
    this.MenuName = producto.producto_nombre;
    this.MenuValue = producto.precio;
    this.RestauranteId = producto.id_restaurante;
    this.formaValida = true;
    this.formaCantidad.value.Cantidad = 1;


    return (<HTMLInputElement>document.getElementById("Cantidad")).value = '1';
  
  }

  vistaCantidad(){
    if(  this.formaCantidad.value.Cantidad !== undefined && 
      this.formaCantidad.value.Cantidad > 0){
        this.formaValida = true;
        return  true;
    }else{
      this.formaValida = false;
      return false;
 }

  }

  cantidad(){

    let date =  new Date();
    let id_card = date.getDate()+''+(date.getMonth()+1)+''+date.getFullYear()+''+
                  date.getHours()+''+date.getMinutes()+''+date.getSeconds()+''+date.getMilliseconds();

    //console.log(id_card);

    if(  this.formaCantidad.value.Cantidad !== undefined && 
         this.formaCantidad.value.Cantidad > 0){
           this.formaValida = true;
        return  true;
    }else{
        this.formaValida = false;
        return false;
    }

  }




  agregarCarrito(){

    if(this.formaValida === false){
      return false;
    }

    const modal:any|null=document.getElementById('modal2');
  

    let orden:any ={
      'id_producto':this.ProductId,
      'valor':this.MenuValue,
      'cantidad':this.formaCantidad.value.Cantidad,
      'nombre': this.MenuName,
      'id_restaurante':this.RestauranteId
    }



    let update=false;
    let continuar=true;

    if(this.detalle.length==0){
      console.log('no hay detalla');
    }else{
      
      this.detalle.forEach((element:any) => { 
      
        if(element.id_restaurante === orden.id_restaurante){
          console.log('es el mismo restaurante');
        }else{
          continuar=false;
          return;
        }
  
  
      });
   
    }

    console.log(continuar);

    if(!continuar){
      modal.click()

      Swal.fire({
        title: 'Error!',
        text: 'No se pueden agregar menus de diferente restaurantes',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
      return false;
    }


    this.detalle.forEach((element:any) => { 
      
      console.log(element.id_producto);
      console.log(orden.id_producto);
      if(element.id_producto === orden.id_producto){
        element.cantidad = element.cantidad + orden.cantidad;
        update=true;
      }


    });

    
    if(!update){
      this.detalle.push(orden);
    }
    console.log(orden);
    console.log(this.detalle);
    modal.click()
;
    localStorage.setItem('detalle',JSON.stringify(this.detalle));
    Swal.fire('Success','Menu agredado', 'success');
    return true;


  }

  pagar(){
    const modal:any|null = document.getElementById('modal2');
    modal.click();
    this.route.navigate(['/cart']);
   
  }
 
  singleproduct(product:any){
    this.route.navigate(['/producto',product.id_producto]);
    console.log(product);
  }

}
