import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { CRUDProductService } from 'src/app/services/crud-product.service';
import { URL_AWS } from 'src/app/services/shared/shared_url_template';
import { ServiceLogsService } from 'src/app/services/service-logs.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  newproduct:boolean=false;
  editproduct:boolean=false;
  productid:number=0;
  public imagePath='';

  edit:boolean=false;
  files:any;
  filestring: string= "";
  tipo: string = "";
  auxtipo: string = "";

  constructor( private fb: FormBuilder,
               private service:CRUDProductService,
               private servicelog:ServiceLogsService) { 
               }

  urlIMG :string ='../../../assets/img/products'
  restaurants:any=[];
  idrestaurants:any=[];
  products:any=[]
  rutaimagen:string="../../../assets/img/food.png"
  auxpath:string='';

  ngOnInit(): void {
  
    
    this.service.getProducts().subscribe(
      (data:any)=>{
        console.log(data);
        this.products=data.data;
        console.log(this.products);
      }
    )
  
    this.service.getRestaurants().subscribe(
      (data:any)=>{
        this.restaurants=data.data;
        this.IDrestaurant();
      },
      err=>{
        console.log(err)
      }
    );

     

  }


  IDrestaurant(){

    this.restaurants.forEach((element:any) => { 
    
      let str = element.id_usuario;
      console.log(str);
      //let splitted = str.split("-", 1); 
      //this.idrestaurants.push(splitted[0]);
    });

    console.log(this.idrestaurants);

  }

  public CreateProductForm = this.fb.group({
    name:['',Validators.required],
    price:['',Validators.required],
    description:['',Validators.required],
    file:['',Validators.required],
    restaurant:['',Validators.required],
    oferta:['']
  });

  public EditProductForm = this.fb.group({
    name:['',Validators.required],
    price:['',Validators.required],
    description:['',Validators.required],
    file:['',Validators.required],
    restaurant:['',Validators.required],
    oferta:['',Validators.required]
  });

  NewProduct(){
    this.newproduct=true;
    this.editproduct=false;
    this.productid=0;
    this.edit=true;

  }


  CreateProduct(){
    console.log('formulario');
    console.log(this.CreateProductForm);
    if(this.CreateProductForm.invalid){
      console.log('error');  
    }

    let str = this.CreateProductForm.controls.restaurant.value;
    console.log(str); 
    let splitted = str.split("-", 1); 

    this.edit=false;
    this.newproduct=false;this.editproduct=false; this.productid=0; this.auxpath='';
    
    this.service.NewProduct(this.CreateProductForm.controls.name.value,
                            this.CreateProductForm.controls.price.value,
                            this.CreateProductForm.controls.description.value,
                            this.filestring,
                            this.auxtipo,
                            splitted[0]).subscribe(
                              (data:any)=>{
                                console.log(data);
                                Swal.fire({
                                  title: 'Success',
                                  text: data.message,
                                  icon: 'success',
                                  confirmButtonText: 'OK'
                                });

                                let usuario:any = localStorage.getItem('username') || '';
                                let id_usuario: any = localStorage.getItem('id_usuario') || 0;
                                let tipo:any = localStorage.getItem('tipo') || 2;
                                let aux_tipo:String='';
                                if(tipo=='1'){
                                  aux_tipo='Restaurante'
                                }else{aux_tipo='Cliente'}
                            
                                if(usuario=='admin'){aux_tipo='Administrado';} 
                            
                                this.servicelog.insertlog(2,'El usuario '+usuario+' creo el producto '+ this.CreateProductForm.controls.name.value,
                                id_usuario,usuario,aux_tipo,'microservice-create-product').subscribe(
                                  data=>{console.log(data)},err=>{console.log(err)}
                                )
   
                              
                              },
                              err=>{
                                Swal.fire({
                                  title: 'Error!',
                                  text: 'Ocurrio un error',
                                  icon: 'error',
                                  confirmButtonText: 'Ok'
                                })
                              }

                            );
  
  }

  EditProduct(product:any){


   // console.log(product);
    this.edit=true;
    this.editproduct=true;
    this.newproduct=false;

    this.imagePath=product.fotografia;
    this.auxpath=URL_AWS+this.imagePath;
    
    //console.log(this.imagePath);
    //console.log(this.CreateProductForm);
    this.CreateProductForm.controls.name.setValue(product.producto_nombre);
    this.CreateProductForm.controls.description.setValue(product.descripcion);
    //this.CreateProductForm.controls.file.setValue(product.fotografia);
    this.CreateProductForm.controls.price.setValue(product.precio);
    
    let element:any = document.getElementById('restaurant');
    
    this.productid=product.id_producto;


        
  }

  UpdateProduct(){


  
    console.log(this.CreateProductForm);
    if(this.CreateProductForm.invalid){
      console.log('error');  
    }

    let str = this.CreateProductForm.controls.restaurant.value; 
    let splitted = str.split("-", 1); 
    this.edit=false;
    this.newproduct=false;this.editproduct=false; this.auxpath=''
     
    //console.log(this.CreateProductForm.controls.name.value);

    this.service.updateproduct(this.productid,
                            this.CreateProductForm.controls.name.value,
                            this.CreateProductForm.controls.price.value,
                            this.CreateProductForm.controls.description.value,
                            this.imagePath,
                            splitted[0],
                            this.CreateProductForm.controls.oferta.value).subscribe(
                              (data:any)=>{
                                console.log(data);
                                Swal.fire({
                                  title: 'Success',
                                  text: 'Producto Actualizado',
                                  icon: 'success',
                                  confirmButtonText: 'OK'
                                });


                                let usuario:any = localStorage.getItem('username') || '';
                                let id_usuario: any = localStorage.getItem('id_usuario') || 0;
                                let tipo:any = localStorage.getItem('tipo') || 2;
                                let aux_tipo:String='';
                                if(tipo=='1'){
                                  aux_tipo='Restaurante'
                                }else{aux_tipo='Cliente'}
                            
                                if(usuario=='admin'){aux_tipo='Administrado';} 
                            
                                this.servicelog.insertlog(3,'El usuario '+usuario+' actualizo el producto '+ this.CreateProductForm.controls.name.value,
                                id_usuario,usuario,aux_tipo,'microservice-edit-product').subscribe(
                                  data=>{console.log(data)},err=>{console.log(err)}
                                )







                              },
                              err=>{
                                Swal.fire({
                                  title: 'Error!',
                                  text: 'Ocurrio un error',
                                  icon: 'error',
                                  confirmButtonText: 'Ok'
                                })
                              }

                            );
    
  }

  DeleteProduct(product:any,index:number){
    console.log(product);

    Swal.fire({
      title: 'Esta Seguro?',
      text: 'Esta a punto de eliminar un Producto',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar'
    }).then((result) => {
      if (result.value) {


        this.service.deleteproduct(product.id_producto).subscribe(
          (data:any)=>{
            Swal.fire({
              title: 'Success',
              text: 'Producto Eliminado',
              icon: 'success',
              confirmButtonText: 'OK'
            })

              this.products.splice(index, 1);

          },
          err=>{
            Swal.fire({
              title: 'Error!',
              text: 'Error no se pudo eliminar',
              icon: 'error',
              confirmButtonText: 'Ok'
            })
          }
        );

      }
    });





  }


  UploadImage(file:any){
    let name =new Date().getFullYear()+''+ new Date().getMonth()+''+ new Date().getDay()+
              ''+new Date().getHours() +''+ new Date().getMinutes()+''+new Date().getSeconds(); 
    console.log(name);
     
    let imagen =file.target.files[0];
    console.log(imagen);

    const formData = new FormData();
       formData.append('image', imagen, name);
       console.log(formData);
  }

  

  seleccionimagen(event:any) {
    this.tipo  = event.target.files[0].type;
    this.auxtipo = this.tipo.substr(6);
    //console.log(this.auxtipo);
    this.files = event.target.files;
    const reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsBinaryString(this.files[0]);
    //console.log(tipo);
  }
  
  _handleReaderLoaded(readerEvt:any) {
    const binaryString = readerEvt.target.result;
    this.filestring = btoa(binaryString);  // Converting binary string data.
    //console.log(this.filestring);

  }



}