import { Router } from '@angular/router'
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CRUDProductService } from 'src/app/services/crud-product.service';
import { URL_AWS } from 'src/app/services/shared/shared_url_template';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sigleproduct',
  templateUrl: './sigleproduct.component.html',
  styleUrls: ['./sigleproduct.component.css']
})
export class SigleproductComponent implements OnInit {

  producto:any;
  detalle:any=[];
  url=URL_AWS;
  constructor(private activateRoute: ActivatedRoute,
              private server:CRUDProductService,
              private route:Router) { }



  ngOnInit(): void {
    const id = this.activateRoute.snapshot.params.id;
    console.log(id);

    this.server.getProduct(id).subscribe(
      (data:any)=>{
        this.producto=data.data;
        this.url=URL_AWS+this.producto.fotografia;
        console.log(this.producto);
      },err=>{console.log(err)}
    )

  }

  regresar(){
    this.route.navigate(['/dashboard']);
  }

  agregarCarrito(){



    const modal:any|null=document.getElementById('modal2');
  

    let orden:any ={
      'id_producto':this.producto.id_producto,
      'valor':this.producto.precio,
      'cantidad':1,
      'nombre': this.producto.nombre,
      'id_restaurante':this.producto.id_restaurante
    }



    let update=false;
    let continuar=true;

    if(this.detalle.length==0){
      console.log('no hay detalle');
    }else{
      
      this.detalle.forEach((element:any) => { 
      
        if(element.id_restaurante === orden.id_restaurante){
          console.log('es el mismo restaurante');
        }else{
          continuar=false;
          return;
        }
  
  
      });
   
    }

    console.log(continuar);

    if(!continuar){
      modal.click()

      Swal.fire({
        title: 'Error!',
        text: 'No se pueden agregar menus de diferente restaurantes',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
      return false;
    }


    this.detalle.forEach((element:any) => { 
      
      console.log(element.id_producto);
      console.log(orden.id_producto);
      if(element.id_producto === orden.id_producto){
        element.cantidad = element.cantidad + orden.cantidad;
        update=true;
      }


    });

    
    if(!update){
      this.detalle.push(orden);
    }
    console.log(orden);
    console.log(this.detalle);
    localStorage.setItem('detalle',JSON.stringify(this.detalle));
    Swal.fire('Success','Menu agredado', 'success');
    return true;


  }




}
