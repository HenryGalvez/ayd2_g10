import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SigleproductComponent } from './sigleproduct.component';

describe('SigleproductComponent', () => {
  let component: SigleproductComponent;
  let fixture: ComponentFixture<SigleproductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SigleproductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SigleproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
