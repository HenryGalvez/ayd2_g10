import { Component, OnInit } from '@angular/core';
import { CRUDOrderService } from 'src/app/services/crud-order.service';
import { ServiceLogsService } from 'src/app/services/service-logs.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
idauxaux:any=[];
  id_usuario:any=0;
  ordenes:any=[];
  allorders:any=[];
  detalle:any=[];
  total:number=0;
  datahistorial:any=[];
  aux:any=[];
  ordersRestaurant:any=[];
  isRestaurant:boolean=false;
  isUser:boolean=false;
  aux2:any=[];
  aux3:any=[];
  estado:string='';
  id_order:number=0;
  isAdmin:boolean=false;
  

  constructor(private service:CRUDOrderService,private servicelog:ServiceLogsService) {

   }


  ngOnInit(): void {
    
    this.id_usuario=localStorage.getItem("id_usuario") || 0;   

    if(localStorage.getItem('username')=='admin'){
      this.isAdmin=true;
      this.isRestaurant=false;
      this.isUser=false;
      this.service.getAllOrders().subscribe((data:any)=>{




        console.log(data);
        //this.allorders=data.data;

        this.aux3=data.data;
        for (let index = 0; index < this.aux3.length; index++) {
          const element:any = this.aux3[index];
  
          if(element.estado_registro=='1'){
            this.allorders.push(element);
          }
          
        }





      })
    }else{
      this.isAdmin=false;
      this.isRestaurant=false;
      this.isUser=false;
    }


    if(localStorage.getItem('tipo')=='2'){
      this.isRestaurant=true;
      this.isUser=false;    
      this.OrdersRestaurants();
    }else{
      this.isRestaurant=false;
      this.isUser=true;
      this.OrdersUsers();
    }

    


  }




  OrdersUsers(){
    this.service.getOrdersUser(this.id_usuario).subscribe((data:any)=>{
      
      this.aux2=data.data;
      console.log(this.aux2);
      for (let index = 0; index < this.aux2.length; index++) {
        const element:any = this.aux2[index];

        if((element.descripcion =="Pagada" ||  element.descripcion =="Cancelada") && element.estado_registro=='1'){
          this.ordenes.push(element);
        }
        
      }

    })
  }

  OrdersRestaurants(){
    

    this.service.getOrdersRestaurant(this.id_usuario).subscribe((data:any)=>{
  

      this.aux=data.data;

      for (let index = 0; index < this.aux.length; index++) {
        const element:any = this.aux[index];

        if((element.descripcion =="Pagada" ||  element.descripcion =="Cancelada") && element.estado_registro=='1'){
          this.ordersRestaurant.push(element);
        }
        
      }

      


    });
  }


  detail(orden:any){
    
    this.total=0;
    this.service.getOrderDeail(orden.id_orden).subscribe((data:any)=>{
      console.log(data);
      this.detalle=data.data;

      this.detalle.forEach((element:any) => {
        this.total=this.total+element.precio;
        this.id_order=element.id_orden;
      });
     
  
    });


  }

  DeleteOrder(order:any,index:number){

    Swal.fire({
      title: 'Esta Seguro?',
      text: 'Esta a punto de eliminar una Orden',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar'
    }).then((result) => {
      if (result.value) {

        
        this.service.updateOrder(order.id_orden,order.direccion,order.tipo_entrega,order.id_usuario,order.id_estado_orden,0)
        .subscribe(
          (data:any)=>{
            Swal.fire({
              title: 'Success',
              text: 'Producto Eliminado',
              icon: 'success',
              confirmButtonText: 'OK'
            })

              this.allorders.splice(index, 1);

          
              this.servicelog.insertlog(4,'El usuario admin elimino la orden #'+order.id_orden,
              1,'admin','Administrador','service_orders').subscribe(
                data=>{console.log(data)},err=>{console.log(err)}
              )




          },
          err=>{
            Swal.fire({
              title: 'Error!',
              text: 'Error no se pudo eliminar',
              icon: 'error',
              confirmButtonText: 'Ok'
            })
          }
        );

      }
    });



  }

}
