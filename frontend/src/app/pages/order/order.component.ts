import { Component, OnInit } from '@angular/core';
import { CRUDOrderService } from 'src/app/services/crud-order.service';
import { ServiceLogsService } from 'src/app/services/service-logs.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  id_usuario:any=0;
  ordenes:any=[];
  ordersRestaurant:any=[];
  detalle:any=[];
  total:number=0;
  isRestaurant:boolean=false;
  isUser:boolean=false;

  aux:any=[];
  aux2:any=[];
  estado:string='';
  

  constructor(private service:CRUDOrderService,private servicelog:ServiceLogsService) {

   }


  ngOnInit(): void {
    this.id_usuario=localStorage.getItem("id_usuario") || 2;

    

    if(localStorage.getItem('tipo')=='2'){
      this.isRestaurant=true;
      this.isUser=false;   
      this.OrdersRestaurants(); 
    }else{
      this.isRestaurant=false;
      this.isUser=true;
      this.OrdersUser();
      
    }

    

  }


  OrdersRestaurants(){
    this.service.getOrdersRestaurant(this.id_usuario).subscribe((data:any)=>{
      console.log(data);
      this.aux=data.data;

      for (let index = 0; index < this.aux.length; index++) {
        const element:any = this.aux[index];

        if(element.descripcion !="Pagada" && element.descripcion !="Cancelada" ){
          if(element.estado_registro=='1'){
            this.ordersRestaurant.push(element);
          }
          
        }
        
      }


    });
  }


  OrdersUser(){
    this.service.getOrdersUser(this.id_usuario).subscribe((data:any)=>{
      
      this.aux2=data.data;

      for (let index = 0; index < this.aux2.length; index++) {
        const element:any = this.aux2[index];

        if(element.descripcion !="Pagada" && element.descripcion !="Cancelada"){
          
          if(element.estado_registro=='1'){
            this.ordenes.push(element);
          }
        }
        
      }


    });
  }




  detail(orden:any){
    
    this.total=0;
    this.service.getOrderDeail(orden.id_orden).subscribe((data:any)=>{
      console.log(data);
      this.detalle=data.data;

      this.detalle.forEach((element:any) => {
        this.total=this.total+element.precio;
      });
  
    });


  }

  cambio(index:any,orden:any){
    let estado=0;
  let elemento:any = document.getElementById('estado'+index);
  console.log(elemento.value);
  console.log(orden);
    
  if(elemento.value.toString()=='Nueva Orden'){estado=1;} 
  if(elemento.value.toString()=='En Preparacion') {estado=2;}
  if(elemento.value.toString()=='En Camino'){estado=3;}
  if(elemento.value.toString()=='Entregado'){estado=4;}
  if(elemento.value.toString()=='Cancelada'){estado=5;}
  if(elemento.value.toString()=='Pagada'){estado=6;}
  console.log(estado);

  this.service.updateOrder(orden.id_orden,orden.direccion,orden.id_tipo_entrega,orden.id_usuario,estado,1)
    .subscribe((data:any)=>{
      this.ordersRestaurant=[];
      this.aux=[];

      let usuario:any = localStorage.getItem('username') || '';
      let id_usuario: any = localStorage.getItem('id_usuario') || 0;
      let tipo:any = localStorage.getItem('tipo') || 2;
      let aux_tipo:String='';
      if(tipo=='1'){
        aux_tipo='Cliente'
      }else{aux_tipo='Restaurante'}
  
  
      this.servicelog.insertlog(3,'El restaurante '+usuario+' cambio el estado de la orden #'+orden.id_orden + ' a ' + elemento.value,id_usuario,usuario,'Restaurante','service_orders').subscribe(
        data=>{console.log(data)},err=>{console.log(err)}
      )




      this.OrdersRestaurants()
    }  );



}

  cancel(orden:any){
    console.log(orden);
    Swal.fire({
      title: 'Esta Seguro?',
      text: 'Esta a punto de Cancelar tu orden',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, cancelar'
    }).then((result) => {
      if (result.value) {

        this.service.updateOrder(orden.id_orden,orden.direccion,orden.id_tipo_entrega,orden.id_usuario,5,1)
        .subscribe((data:any)=>{
          this.ordenes=[];
          this.aux2=[];
    
          let usuario:any = localStorage.getItem('username') || '';
          let id_usuario: any = localStorage.getItem('id_usuario') || 0;
          let tipo:any = localStorage.getItem('tipo') || 2;
          let aux_tipo:String='';
          if(tipo=='1'){
            aux_tipo='Cliente'
          }else{aux_tipo='Restaurante'}
      
      
          this.servicelog.insertlog(3,'El usuario '+usuario+' cambio el estado de la orden #'+orden.id_orden + ' a cancelado' ,id_usuario,usuario,'Usuario','service_orders').subscribe(
            data=>{console.log(data)},err=>{console.log(err)}
          )
    
    
    
    
          this.OrdersUser()
        }  );




            Swal.fire({
              title: 'Success',
              text: 'Orden Cancelada',
              icon: 'success',
              confirmButtonText: 'OK'

            })
                     }
  })
  }

}
