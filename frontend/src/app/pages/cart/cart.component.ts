import { Component, OnInit } from '@angular/core';
import {  Validators, FormBuilder } from '@angular/forms';

import { CRUDOrderService } from 'src/app/services/crud-order.service';
import { ServiceLogsService } from 'src/app/services/service-logs.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  detalle:any=[];
  Total:number=0;
  direccion:boolean=false;

    public formOrden = this.fb.group({
    entrega:['',Validators.required],
    direccion:['']
  })
  constructor(private fb:FormBuilder,
              private service:CRUDOrderService,
              private servicelog:ServiceLogsService) { }

  ngOnInit(): void {

    let id_usuario= localStorage.getItem('id_usuario') || 2;
    //console.log(Number(id_usuario));

    if(localStorage.getItem('detalle')){
      this.detalle = JSON.parse(localStorage.getItem('detalle')|| '{}');
      //console.log(this.detalle);
      this.calcularTotal();
    }
    //console.log(this.detalle);
  }


   calcularTotal(){
    this.Total=0;
    if(this.detalle.length > 0){
      this.detalle.forEach((element:any) => {
      this.Total=this.Total+element.valor*element.cantidad;  
      });
    }
    else{
      this.Total=0;
    }
    //console.log(this.Total);
    
  }

  Orden(){


    if(this.direccion==true && this.formOrden.value.direccion =='' ){
      Swal.fire({
        title: 'Error!',
        text: 'Ingrese una direccion',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
      return ;
    }

    if(this.detalle.length==0){

        Swal.fire({
          title: 'Error!',
          text: 'Aun no ha seleccionado un menu',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
        return ;
      
  
    }



    console.log(this.formOrden);

    let tipo_orden:number;
    if(this.formOrden.value.entrega.toString()=="Domicilio"){
     tipo_orden=1;
    }else{
      tipo_orden=2;
    }

    let id_usuario= localStorage.getItem('id_usuario') || 2;
    console.log(Number(id_usuario));

    let id_orden=0;

   this.service.newOrder(this.formOrden.value.direccion,tipo_orden,Number(id_usuario)).
   subscribe((data:any)=>{


   this.detalle.forEach((element:any) => {

    for (let index = 0; index < element.cantidad; index++) {

    this.service.OrderDetail(data.id,element.id_producto).subscribe(data=>{
      console.log(data);
    },err=>{console.log(err)}

    )

    }


    let usuario:any = localStorage.getItem('username') || 'anonymous';
    let id_usuario: any = localStorage.getItem('id_usuario') || 2;



    this.servicelog.insertlog(2,'El usuario '+usuario+' creo una orden',
    id_usuario,usuario,'Cliente','service-orders').subscribe(
      data=>{console.log(data)},err=>{console.log(err)}
    )
  
  });

  Swal.fire({
    title: 'Success',
    text: data.message,
    icon: 'success',
    confirmButtonText: 'OK'
  })

  this.detalle=[];
  localStorage.setItem('detalle','');
  this.Total=0;
  
   },err=>{

    Swal.fire({
      title: 'Error',
      text: 'Ocurrio un error',
      icon: 'error',
      confirmButtonText: 'OK'
    })
   });







}

  cambio(){
    console.log(this.formOrden.value.entrega);
    if(this.formOrden.value.entrega.toString()=="Domicilio"){
      this.direccion=true;
    }else{
      this.direccion=false;
    }
  }

  cancelar(){


    Swal.fire({
      title: 'Esta Seguro?',
      text: 'Esta a punto de Cancelar tu pedido',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, cancelar'
    }).then((result) => {
      if (result.value) {
        this.detalle=[];
        localStorage.setItem('detalle','');
        this.Total=0;
            Swal.fire({
              title: 'Success',
              text: 'Orden Cancelada',
              icon: 'success',
              confirmButtonText: 'OK'

            })
                     }
  })









    
  }


  DeleteMenu(item:any,i:number){
    console.log(item);
    console.log(i);

    Swal.fire({
      title: 'Esta Seguro?',
      text: 'Esta a punto de Eliminar un menu',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, cancelar'
    }).then((result) => {
      if (result.value) {


        this.detalle.pop(i);
        localStorage.setItem('detalle',JSON.stringify(this.detalle));
        this.calcularTotal();
            Swal.fire({
              title: 'Success',
              text: 'Orden Cancelada',
              icon: 'success',
              confirmButtonText: 'OK'

            });
                     }
  })




  }

}
