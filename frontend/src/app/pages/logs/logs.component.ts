import { Component, OnInit } from '@angular/core';
import { ServiceLogsService } from 'src/app/services/service-logs.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  constructor(private service:ServiceLogsService) { }
  logs:any=[];
  ngOnInit(): void {
    this.service.getlogs().subscribe((data:any)=>{
      console.log(data);this.logs=data.data},err=>console.log(err));
  }

}
