import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceLogsService } from 'src/app/services/service-logs.service';
import { SignUpService } from 'src/app/services/signup.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {

  iguales:boolean=true;

  public RestaurantForm = this.fb.group({
    name:['',Validators.required],
    address:['',Validators.required],
    phoneNumber:['',Validators.required],
    email:['',[Validators.required, Validators.email]],
    password:['',Validators.required],
    password2:['',Validators.required]
    
  });
  
  constructor(
    private fb:FormBuilder,
    private service:SignUpService,
    private servicelog:ServiceLogsService
  ) { }





  ngOnInit(): void {
  }
  
  
  createRestaurant(){
    console.log(this.RestaurantForm);
    if (this.RestaurantForm.invalid){
      return false;
    }

    if(this.RestaurantForm.value.password !== this.RestaurantForm.value.password2){
      this.iguales=false;

      return false;
    }else{
      this.iguales=true;

      this.service.signup(this.RestaurantForm.value.email,
                          this.RestaurantForm.value.password,
                          this.RestaurantForm.value.name,
                          this.RestaurantForm.value.phoneNumber,
                          this.RestaurantForm.value.address,'2')
                          .subscribe((data:any)=>{

                            Swal.fire({
                                title: 'Success',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'OK'
                              });
                              this.servicelog.insertlog(2,'EL usuario admin creo el restaurante ' +this.RestaurantForm.value.name,
                              1,'admin','Administrador','services users').subscribe(data=>console.log(data));
                              
                            },
                            err=>{
                              Swal.fire({
                                title: 'Error!',
                                text: 'Ocurrio un error',
                                icon: 'error',
                                confirmButtonText: 'Ok'
                              })
                            }
                          );



      return true;
    }



  }

}
