import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_AUTH } from '../services/shared/shared_url_template'
@Injectable({
  providedIn: 'root'
})
export class SignUpService {

  constructor(private http:HttpClient) { 

  }

  signup(Email:string, Password: string,Name:string,Phone:string,Address:string,tipo:string){
    const user={
      nombre:Name,
      telefono:Phone,
      direccion:Address,
      usuario:Email,
      password:Password,
      id_rol:tipo
    };
    const url = URL_AUTH + '/users/signup-user';
    return this.http.post(url,user);

  }




}

