import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_CREATE_PRODUCT, URL_DELETE_PRODUCT, URL_EDIT_PRODUCT, URL_GET_PRODUCT, URL_LIST_PRODUCT, URL_SERVER2 } from './shared/shared_url_template';

@Injectable({
  providedIn: 'root'
})
export class CRUDProductService {

  constructor(private http:HttpClient) { 

  }

  NewProduct(name:string, price:number, description:string, file:string,tipo:string, restaurant:number){

    const product={
      nombre:name,
      precio:price,
      descripcion:description,
      fotografia:file,
      extension:tipo,
      id_restaurante: restaurant
    };
    console.log(product)


    const url = URL_CREATE_PRODUCT + '/products/create-product';
    return this.http.post(url,product);

  }

  getRestaurants(){
    const url = URL_LIST_PRODUCT + '/list_products/list/restaurants'
    return this.http.get(url);
  }

  getProducts(){
    const url = URL_LIST_PRODUCT + '/list_products/list/products'
    return this.http.get(url);
  }

  getOferts(){
    const url = URL_LIST_PRODUCT + '/list_products/list/oferts'
    return this.http.get(url);
  }


  getProduct(id:string){
    const url = URL_GET_PRODUCT + '/products/get-product/'+id
    console.log(url);
    return this.http.get(url);
  }

  updateproduct(id:number,name:string, price:number, description:string, file:string, restaurant:string,ofert:string){
    

    const product={
      nombre:name,
      precio:price,
      descripcion:description,
      fotografia:file,
      id_restaurante: restaurant,
      oferta: ofert
    };

    console.log(product);

    const url = URL_EDIT_PRODUCT + '/products/update-producto/'+id
    return this.http.put(url,product);


  }

  deleteproduct(id:number){
    const url = URL_DELETE_PRODUCT + '/products/delete-producto/'+id;
    return this.http.delete(url);

  }

}
