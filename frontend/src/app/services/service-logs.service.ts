import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_LOGS } from './shared/shared_url_template';

@Injectable({
  providedIn: 'root'
})
export class ServiceLogsService {

  constructor(private http:HttpClient) { }


  insertlog(id_tipo_log:Number,descripcion:String,id_usuario_realizo:Number,
            nombre_usuario:String,nivel_accion:String,origen:String){

    const log={
      id_tipo_log:id_tipo_log,
      descripcion:descripcion,
      id_usuario_realizo:id_usuario_realizo,
      nombre_usuario:nombre_usuario,
      nivel_accion:nivel_accion,
      origen:origen
    };
    console.log(log);
    const url = URL_LOGS + '/users/logs';
    return this.http.post(url,log);

  


  }

  getlogs(){
    const url = URL_LOGS+'/users/get-all-logs';
    return this.http.get(url);
  }


}
