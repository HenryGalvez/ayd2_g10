import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_ORDERS } from './shared/shared_url_template';

@Injectable({
  providedIn: 'root'
})
export class CRUDOrderService {

  constructor(private http:HttpClient) { }

  newOrder(direccion:string, tipo_entrega:number, id_usuario:number){


    const order={
      direccion:direccion || 'Recoger en tienda',
      id_tipo_entrega:tipo_entrega,
      id_usuario:id_usuario,
      id_estado_orden:1,
      
    };
    console.log(order)


    const url = URL_ORDERS + '/orders/create-order';
    return this.http.post(url,order);

  }

  OrderDetail(id_order:number,id_producto:number){


    const order={
      id_orden:id_order,
      id_producto:id_producto
    };
    console.log(order)

    const url = URL_ORDERS + '/orders/insert-item-order';
    return this.http.post(url,order);

  }

  getAllOrders(){
    const url = URL_ORDERS + '/orders/get-all-orders';
    return this.http.get(url);
  }

  getOrdersUser(id_usuario:number){
    const url = URL_ORDERS + '/orders/get-orders/'+id_usuario;
    return this.http.get(url);
  }

  getOrderDeail(id_order:number){
    const url = URL_ORDERS + '/orders/get-detail-order/'+id_order;
    return this.http.get(url);

  }

  getOrdersRestaurant(id_restaurante:number){
    const url = URL_ORDERS + '/orders/get-orders-restaurant/'+id_restaurante;
    return this.http.get(url);
  }

  updateOrder(id_order:number,direccion:string, tipo_entrega:number, id_usuario:number,id_estado_orden:number,estado_registro:number){

      const order={
        direccion:direccion || 'Recoger en tienda',
        id_tipo_entrega:tipo_entrega,
        id_usuario:id_usuario,
        id_estado_orden:id_estado_orden,
        estado_registro:estado_registro 
      };

      const url = URL_ORDERS+'/orders/update-order/'+id_order;

      return this.http.put(url,order);


  }

  deleteOrder(id_order:number){
    const url = URL_ORDERS+'/orders/delete-order/'+id_order;

    return this.http.delete(url);
  }




}
