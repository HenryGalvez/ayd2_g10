import { TestBed } from '@angular/core/testing';

import { CRUDOrderService } from './crud-order.service';

describe('CRUDOrderService', () => {
  let service: CRUDOrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CRUDOrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
