import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { PagenotfoundComponent } from './pages/pagenotfound/pagenotfound.component';
import { ProductComponent } from './pages/product/product.component';
import { CartComponent } from './pages/cart/cart.component';
import { RestaurantComponent } from './pages/restaurant/restaurant.component';
import { HistoryComponent } from './pages/history/history.component';
import { OrderComponent } from './pages/order/order.component';
import { LogsComponent } from './pages/logs/logs.component';
import { SigleproductComponent } from './pages/sigleproduct/sigleproduct.component';

const routes: Routes = [
  {path:'dashboard', component: DashboardComponent},
  {path:'product', component: ProductComponent},
  {path:'history', component: HistoryComponent},
  {path:'logs', component: LogsComponent},
  {path:'cart', component:CartComponent },
  {path:'restaurant', component:RestaurantComponent },
  {path:'order', component:OrderComponent },
  {path:'producto/:id', component:SigleproductComponent },
  
  {path:'', redirectTo: '/dashboard', pathMatch: 'full' },
  {path:'**', component:PagenotfoundComponent}
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})




export class AppRoutingModule { }
