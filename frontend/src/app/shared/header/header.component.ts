import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { ServiceLogsService } from 'src/app/services/service-logs.service';
import { SignUpService } from 'src/app/services/signup.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  mensaje=".";
  hidelogin=true;
  isAdmin=false;
  userAnonymous=false;
  username:any=localStorage.getItem('nombre') || 'anonimo'
  public loginForm = this.fb.group({
    userName:['',Validators.required],
    password:['',Validators.required]
  })

  public signupForm = this.fb.group({
    email:['',Validators.required],
    password:['',Validators.required],
    name:['',Validators.required],
    phone:['',Validators.required],
    address:['',Validators.required]
  })
  
  constructor(
    private fb:FormBuilder,
    public service:LoginService,
    public service2: SignUpService,
    public servicelog:ServiceLogsService
  ) { }

  ngOnInit(): void {
    this.postLogin();
    
    console.log(window.location.hostname);
  }
 
  ClearLogin(){
    this.loginForm.setValue({userName:'',password:''});
    this.mensaje="";
    }

  Login(){
    console.log(this.loginForm);
    if(this.loginForm.invalid){
      console.log('error');
    }
    this.service.login(this.loginForm.value.userName, this.loginForm.value.password).subscribe(
      (data:any)=>{
        if(data[0].nombre.length>0){
          localStorage.setItem('username',data[0].nombre);
          localStorage.setItem('id_usuario',data[0].id_usuario);
          localStorage.setItem('tipo',data[0].id_rol)
          localStorage.setItem('nombre',data[0].nombre);
          this.username=localStorage.getItem('nombre') || 'anonimo'
          
          console.log(data);
          this.hidelogin=false;
          var element = document.getElementById("modal");
          element?.click();
          
          let tipo:String="";
          if(data[0].id_rol==1){
            tipo="Cliente"; 
          }else{
            tipo="Restaurante";
          }


          if(data[0].nombre =='admin'){
            this.isAdmin=true;
            this.userAnonymous=false;
            tipo="Administrador"
          }else
          {
            this.isAdmin=false;
            this.userAnonymous=false;
          }
          
          

          this.servicelog.insertlog(5,'El usuario '+data[0].nombre+' inicio sesion',data[0].id_usuario,
                                    data[0].nombre,tipo,'service-users').subscribe(data=>{
                                      console.log(data)
                                    },err=>console.log(data));
          

        }
        
      },
      err=>{
        console.log(err);
        this.mensaje=err.error.message || 'Error';
       
      }
    )

  }

  postLogin(){
    let auxuser = localStorage.getItem('username') || '';
    if(auxuser.length>0){
      this.userAnonymous=false;
      console.log(auxuser);
      
      if(auxuser=='admin'){
        this.isAdmin=true;
        this.hidelogin=false;
      }else{
        this.hidelogin=false;
        this.isAdmin=false;
      }
      

    }else{this.userAnonymous=true;}
  }

  Logout(){
    
    let usuario:any = localStorage.getItem('username') || '';
    let id_usuario: any = localStorage.getItem('id_usuario') || 0;
    let tipo:any = localStorage.getItem('tipo') || 2;
    let aux_tipo:String='';
    if(tipo=='1'){
      aux_tipo='Clinete'
    }else{aux_tipo='Restaurante'}

    if(this.isAdmin==true){aux_tipo='Administrado';} 

    this.servicelog.insertlog(6,'El usuario '+usuario+' cerro sesion',id_usuario,usuario,aux_tipo,'service_users').subscribe(
      data=>{console.log(data)},err=>{console.log(err)}
    )
    this.hidelogin=true;
    this.isAdmin=false;
    this.userAnonymous=true;
    localStorage.removeItem('username');
    localStorage.removeItem('detalle');
    localStorage.removeItem('id_usuario');
    localStorage.removeItem('nombre');
    this.username='anonimo'


  }

  SignUp(){
    console.log(this.signupForm);
    if(this.signupForm.invalid){
      console.log('error');
    }
    this.service2.signup(this.signupForm.value.email, this.signupForm.value.password,this.signupForm.value.name,this.signupForm.value.phone,this.signupForm.value.address,'1').subscribe(
      (data:any)=>{
        console.log(data);
       
          var element = document.getElementById("signup_modal");
          element?.click();
          Swal.fire({
            title: 'Success',
            text: data.message,
            icon: 'success',
            confirmButtonText: 'OK'
          });


      
          this.servicelog.insertlog(2,'Se creo el usuario '+this.signupForm.value.email,
          data.id,this.signupForm.value.name,'Cliente','service users').subscribe(
            data=>{console.log(data)},err=>{console.log(err)}
          )






        
        
      },
      err=>{
        console.log(err);
        this.mensaje="error en signup"
       
      }
    )

  }

}
