import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
 @Input() isAdmin:Boolean = false;
 @Input() userAnonymous:Boolean = false;
 @Input() username:string = "Anonimo";
  constructor() { }

  ngOnInit(): void {
    console.log(this.userAnonymous);
  }

}
