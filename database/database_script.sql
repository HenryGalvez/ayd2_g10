CREATE DATABASE ayd2;
USE ayd2;

/*CREACION DE TABLAS CATALOGO*/

CREATE TABLE TC_ROL(
	id_rol INT PRIMARY KEY,
    descripcion VARCHAR(50)
);

CREATE TABLE TC_TIPO_ENTREGA(
	id_tipo_entrega INT PRIMARY KEY,
    descripcion VARCHAR(100)
);

CREATE TABLE TC_ESTADO_ORDEN(
	id_estado_orden INT PRIMARY KEY,
    descripcion VARCHAR(100)
);

CREATE TABLE TC_TIPO_LOG(
	id_tipo_log INT PRIMARY KEY,
    descripcion VARCHAR(50)
);


/*LLENADO DE LOS CATALOGOS*/

INSERT INTO TC_ROL(id_rol, descripcion) VALUES(1, 'Restaurante');
INSERT INTO TC_ROL(id_rol, descripcion) VALUES(2, 'Clienta');

INSERT INTO TC_TIPO_ENTREGA(id_tipo_entrega, descripcion) VALUES(1, 'A Domicilio');
INSERT INTO TC_TIPO_ENTREGA(id_tipo_entrega, descripcion) VALUES(2, 'Recoger en Tienda');

INSERT INTO TC_ESTADO_ORDEN(id_estado_orden, descripcion) VALUES(1, 'Nueva Orden');
INSERT INTO TC_ESTADO_ORDEN(id_estado_orden, descripcion) VALUES(2, 'En Preparacion');
INSERT INTO TC_ESTADO_ORDEN(id_estado_orden, descripcion) VALUES(3, 'En Camino');
INSERT INTO TC_ESTADO_ORDEN(id_estado_orden, descripcion) VALUES(4, 'Entregado');
INSERT INTO TC_ESTADO_ORDEN(id_estado_orden, descripcion) VALUES(5, 'Cancelada');
INSERT INTO TC_ESTADO_ORDEN(id_estado_orden, descripcion) VALUES(6, 'Pagada');

INSERT INTO TC_TIPO_LOG(id_tipo_log, descripcion) VALUES(1, 'Operacion');
INSERT INTO TC_TIPO_LOG(id_tipo_log, descripcion) VALUES(2, 'Creacion');
INSERT INTO TC_TIPO_LOG(id_tipo_log, descripcion) VALUES(3, 'Edicion');
INSERT INTO TC_TIPO_LOG(id_tipo_log, descripcion) VALUES(4, 'Eliminacion');
INSERT INTO TC_TIPO_LOG(id_tipo_log, descripcion) VALUES(5, 'Logueo');
INSERT INTO TC_TIPO_LOG(id_tipo_log, descripcion) VALUES(6, 'Logout');
INSERT INTO TC_TIPO_LOG(id_tipo_log, descripcion) VALUES(7, 'Error');
/*CREACION DE TABLAS TRANSACCIONALES*/

CREATE TABLE TT_USUARIO(
	id_usuario INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    telefono VARCHAR(12) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    usuario VARCHAR(75) NOT NULL,
    password VARCHAR(150) NOT NULL,
    id_rol INT NOT NULL,
    CONSTRAINT fk_tt_usuario_tc_rol FOREIGN KEY (id_rol) REFERENCES TC_ROL(id_rol)
);

CREATE TABLE TT_PRODUCTO(
	id_producto INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    precio DECIMAL NOT NULL,
    descripcion VARCHAR(255),
    fotografia VARCHAR(255) NOT NULL,
    id_restaurante INT NOT NULL,
    CONSTRAINT fk_tt_producto_tt_usuario FOREIGN KEY (id_restaurante) REFERENCES TT_USUARIO(id_usuario)
);

CREATE TABLE TT_ORDEN(
	id_orden INT AUTO_INCREMENT PRIMARY KEY,
    fecha_hora DATETIME NOT NULL,
    direccion VARCHAR(255),
    id_tipo_entrega INT NOT NULL,
    id_usuario INT,
    id_estado_orden INT NOT NULL,
    estado_registro INT NOT NULL,
    CONSTRAINT fk_tc_tipo_entrega_tt_orden FOREIGN KEY (id_tipo_entrega) REFERENCES TC_TIPO_ENTREGA(id_tipo_entrega),
    CONSTRAINT fk_tt_usuario_tt_orden FOREIGN KEY (id_usuario) REFERENCES TT_USUARIO(id_usuario),
    CONSTRAINT fk_tc_estado_orden_tt_orden FOREIGN KEY (id_estado_orden) REFERENCES TC_ESTADO_ORDEN(id_estado_orden)
);

CREATE TABLE TT_DETALLE_ORDEN(
	id_detalle_orden INT AUTO_INCREMENT PRIMARY KEY,
    id_orden INT NOT NULL,
    id_producto INT NOT NULL,
    CONSTRAINT fk_tt_detalle_tt_orden FOREIGN KEY (id_orden) REFERENCES TT_ORDEN(id_orden),
    CONSTRAINT fk_tt_detalle_tt_producto FOREIGN KEY (id_producto) REFERENCES TT_PRODUCTO(id_producto)
);

CREATE TABLE TT_LOG(
	id_log INT AUTO_INCREMENT PRIMARY KEY,
    id_tipo_log INT NOT NULL,
    descripcion VARCHAR(255) NOT NULL,
    id_usuario_realizo INT NOT NULL,
    nombre_usuario VARCHAR(255) NOT NULL,
    nivel_accion VARCHAR (150) NOT NULL,
    origen VARCHAR(75) NOT NULL,
    fecha_hora DATETIME NOT NULL,
    CONSTRAINT fk_tt_log_tc_tipo_log FOREIGN KEY (id_tipo_log) REFERENCES TC_TIPO_LOG(id_tipo_log),
    CONSTRAINT fk_tt_log_tt_usuario FOREIGN KEY (id_usuario_realizo) REFERENCES TT_USUARIO(id_usuario)
);

INSERT INTO TT_USUARIO (nombre,telefono,direccion,usuario,password,id_rol) VALUES('admin','47112405','Guatemala','admin','$2b$09$k/rWcLUdLsYkO0P3eL92MuoHVJpMaiRUVNzqu8RVrVTm.HlrhX3v6',2);
INSERT INTO TT_USUARIO (nombre,telefono,direccion,usuario,password,id_rol) VALUES('anonimo','10000000','Guatemala','anonimo','$2b$09$k/rWcLUdLsYkO0P3eL92MuoHVJpMaiRUVNzqu8RVrVTm.HlrhX3v6',1);

ALTER TABLE `TT_PRODUCTO` ADD `oferta` FLOAT NOT NULL DEFAULT 0.0;

ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'password';
flush privileges;