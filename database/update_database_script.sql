USE ayd2;

/*
estado_registro = 1 -> activo
estado_registro = 0 -> inactivo
*/

ALTER TABLE `TT_ORDEN` ADD `estado_registro` INT NOT NULL DEFAULT 1;

ALTER TABLE `TT_PRODUCTO` ADD `oferta` FLOAT NOT NULL DEFAULT 0.0;