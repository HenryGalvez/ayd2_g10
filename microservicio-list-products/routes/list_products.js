const conn = require("../private/database");
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});


router.get('/list/restaurants', function ( req,res, next) {

  const query = "select * from TT_USUARIO where id_rol = 2 AND usuario!='admin'"
    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
      }
      res.status(200).json({data: rows})
    })
});

router.get('/list/products', function ( req,res, next) {

  const query = `select p.id_producto,p.nombre as 'producto_nombre',p.precio,
                 p.descripcion, p.fotografia, tu.nombre as 'nombre_restaurante',
                 p.id_restaurante
                 from TT_PRODUCTO p,TT_USUARIO tu where p.id_restaurante =tu.id_usuario
                 ORDER by tu.nombre;`
    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
      }
      res.status(200).json({data: rows})
    })
});

router.get('/list/oferts', function ( req,res, next) {

  const query = `select p.id_producto,p.nombre as 'producto_nombre',p.precio,
                  p.descripcion, p.fotografia, tu.nombre as 'nombre_restaurante',
                  p.id_restaurante,
                  p.oferta
                  from TT_PRODUCTO p,TT_USUARIO tu where p.id_restaurante =tu.id_usuario
                  and oferta>0
                  ORDER by tu.nombre;`
    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
      }
      res.status(200).json({data: rows})
    })
});







router.get('/list/:id_restaurante', function (req, res, next) {
  var {
    id_restaurante,
  } = req.params;
  const query = `select * from TT_PRODUCTO where id_restaurante = ${id_restaurante}`
    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
      }
      res.status(200).json({data: rows})
    })
});



module.exports = router;
