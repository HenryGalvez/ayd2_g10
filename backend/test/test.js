'use stricts'

let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3001';


// Prueba correcta
describe('Registro de un nuevo usuario: ',()=>{
    it('should insert a user',  (done) => {
    chai.request(url)
    .post('/users/signup-user')
    .send({nombre:"User Test", telefono: "12345678", direccion: "Testing city", usuario: 'test', password: 'test', id_rol:1})
    .end( function(err,res){
    //console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
});

//Prueba incorrecta

describe('Registro de un nuevo usuario: ',()=>{
    it('should insert a user',  (done) => {
    chai.request(url)
    .post('/users/signup-user')
    .send({ direccion: "Testing city", usuario: 'test', password: 'test', id_rol:1})
    .end( function(err,res){
    expect(res).to.have.status(200);
    done();
    });
    });
});