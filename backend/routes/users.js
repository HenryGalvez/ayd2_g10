const conn = require("../private/database");
let bcrypt = require('bcrypt');
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

router.post('/signup-user', function (req, res, next) {
  var {
    nombre,
    telefono,
    direccion,
    usuario,
    password,
    id_rol
  } = req.body;
  console.log(nombre,telefono,direccion,usuario,password);
  bcrypt.hash(password, 9, (err, hash)=>{
    if(err){
      res.status(500).json({ message: err.message});
    }
    const query = `insert into TT_USUARIO(nombre, telefono, direccion, usuario, password, id_rol) values('${nombre}', '${telefono}', '${direccion}', '${usuario}', '${hash}', ${id_rol})`
    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: err.message});
      }
      conn.query("select last_insert_id()", (err, rows, fields) => {
        if(err){
          res.status(500).json({ message: err.message});
        }
        res.status(200).json({message: "Se registró correctamente el usuario", id: rows[0]['last_insert_id()']})
      })
    })
    
  })
});


//Login de usuario
router.post('/login-user',(req,res)=>{
  var usuario = req.body.usuario;
  var password = req.body.password;

  if (usuario && password) {
    console.log("user: "+usuario+" pass: "+password);
    conn.query('SELECT * FROM TT_USUARIO WHERE usuario = ?', [usuario], 
      (err, results, fields)=> {
        if(results[0] != null){
          const dcryptPassword = bcrypt.compareSync(password,results[0].password); // true or false
            if (dcryptPassword) {
            res.status(200).json(results);
            } else {
              res.status(500).json({message: 'Password Incorrecta'})
              res.send();
            }            
        res.end();

        }else{
          res.status(500).json({message: 'El usuario no esta registrado!'})
        }
        
    });
  } else {
    res.send('Ingrese Username y Password!');
    res.end();
  }

});



router.post('/logs', function (req, res, next) {
  var {
    id_tipo_log,
    descripcion,
    id_usuario_realizo,
    nombre_usuario,
    nivel_accion,
    origen
  } = req.body;

  console.log(req.body);


    const query = `insert into TT_LOG(id_tipo_log,descripcion,id_usuario_realizo,nombre_usuario,nivel_accion,origen,fecha_hora) values('${id_tipo_log}', '${descripcion}', '${id_usuario_realizo}','${nombre_usuario}','${nivel_accion}','${origen}', now())`
    console.log(query);
    conn.query(query, (err, rows, fileds) => {
      if(err){
        res.status(500).json({ message: err.message});
      }
      conn.query("select last_insert_id()", (err, rows, fields) => {
        if(err){
          res.status(500).json({ message: err.message});
        }
        res.status(200).json({message: "Se registro el log", id: rows[0]['last_insert_id()']})
     
      })
    })
    
  });

router.get('/get-all-logs', function (req, res, next) {

    const query = `SELECT tl.*,ttl.descripcion as 'tipo' from TT_LOG tl,TC_TIPO_LOG ttl 
                   where tl.id_tipo_log = ttl.id_tipo_log`
  
      conn.query(query, (err, rows, fileds) => {
        if(err){
          res.status(500).json({ message: "Hubo un error que no pudo controlarse"});
        }
        res.status(200).json({data: rows})
      })
  });
  



module.exports = router;
