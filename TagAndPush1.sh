#!/bin/bash
set -eo pipefail
echo "Subiendo Mis imagenes a DockerHub"
filename="version1"
while read -r line; do
    echo "$line" 
    #VAMOS A TAGUEAR LAS IMAGENES , pueden construirse nuevamente... con docker buil -t
    #docker build -t brandonsoto3/practica2:frontend${line} ./frontend

    

    docker tag ayd2_g10_service-users registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_service-users${line}
    docker tag ayd2_g10_service-orders registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_service-orders${line}
    docker tag ayd2_g10_frontend registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_frontend${line}
    docker tag ayd2_g10_mysql registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_mysql${line}

    
    docker push registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_service-users${line}
    docker push registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_service-orders${line}
    docker push registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_frontend${line}
    docker push registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_mysql${line}


    echo "FIN DEL SCRIPT"
done < "$filename"

