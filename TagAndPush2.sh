#!/bin/bash
set -eo pipefail
echo "Subiendo Mis imagenes a DockerHub"
filename="version2"
while read -r line; do
    echo "$line" 
    #VAMOS A TAGUEAR LAS IMAGENES , pueden construirse nuevamente... con docker buil -t
    #docker build -t brandonsoto3/practica2:frontend${line} ./frontend




    docker tag ayd2_g10_microservicio-get-product registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservicio-get-product${line}
    docker tag ayd2_g10_microservice-edit-product registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservice-edit-product${line}
    docker tag ayd2_g10_microservice-delete-product registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservice-delete-product${line}
    docker tag ayd2_g10_microservicio-list-products registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservicio-list-products${line}
    docker tag ayd2_g10_microservicio-create-product registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservicio-create-product${line}

    
    docker push registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservicio-get-product${line}
    docker push registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservice-edit-product${line}
    docker push registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservice-delete-product${line}
    docker push registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservicio-list-products${line}
    docker push registry.gitlab.com/henrygalvez/ayd2_g10:ayd2_g10_microservicio-create-product${line}


    echo "FIN DEL SCRIPT"
done < "$filename"

