'use stricts'

let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3000';

describe('get the product with id 1: ',()=>{
    it('should get the product with id 1', (done) => {
    chai.request(url)
    .get('/products/get-product/1')
    .end( function(err,res){
    let data = res.body;
    console.log(data.data)
    expect(data.data).to.have.property('id_producto').to.be.equal(1);
    expect(res).to.have.status(200);
    done();
    });
    });
});


describe('get the product with id 1: ',()=>{
    it('should get the product with id 1', (done) => {
    chai.request(url)
    .get('/products/get-product/1')
    .end( function(err,res){
    let data = res.body;
    console.log(data.data)
    expect(data.data).to.have.property('id').to.be.equal(1);
    expect(res).to.have.status(400);
    done();
    });
    });
});






